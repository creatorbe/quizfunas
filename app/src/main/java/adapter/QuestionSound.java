package adapter;

/**
 * Created by creatorbe on 1/3/16.
 */
public class QuestionSound {
    private int id;
    private String nama;
    private String imagefile;
    private String soundfile;

    public int getId() {
        return id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setName( String name ) {
        this.nama = name;
    }

    public String getImagefile() {
        return imagefile;
    }

    public void setImagefile( String imagefile ) {
        this.imagefile = imagefile;
    }

    public String getSoundfile() {
        return soundfile;
    }

    public void setSoundfile( String soundfile ) {
        this.soundfile = soundfile;
    }
}