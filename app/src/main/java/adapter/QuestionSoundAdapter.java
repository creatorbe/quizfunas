package adapter;

import android.content.Context;

import ssk.quizfun.R;

/**
 * Created by creatorbe on 1/3/16.
 */
public class QuestionSoundAdapter {
    private QuestionSound[] hurufs;

    public QuestionSoundAdapter( Context context ) {
        loadHurufsFromResources( context );
    }

    public QuestionSound[] getHurufArray() {
        return this.hurufs;
    }

    private void loadHurufsFromResources( Context context ) {
        String[] hurufNames = context.getResources().getStringArray( R.array.questionSound );
        //String[] hurufImages = context.getResources().getStringArray( R.array.hijaiyah_images );
        String[] hurufSounds = context.getResources().getStringArray( R.array.questionSound );

        hurufs = new QuestionSound[hurufSounds.length];

        for ( int hurufIndex = 0; hurufIndex < hurufNames.length; hurufIndex++ ) {
            QuestionSound huruf = new QuestionSound();

            //huruf.setName( hurufNames[hurufIndex] );
            //huruf.setImagefile( hurufImages[hurufIndex] );
            huruf.setSoundfile( hurufSounds[hurufIndex] );

            hurufs[hurufIndex] = huruf;
        }
    }
}
