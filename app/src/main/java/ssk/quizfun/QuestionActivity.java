package ssk.quizfun;


import java.io.IOException;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.app.AlertDialog.Builder;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;

import android.util.Log;
import android.view.View.OnClickListener;
import android.view.Window;

import android.view.View;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import adapter.QuestionSound;

public class QuestionActivity extends Activity implements OnClickListener{
	/** Called when the activity is first created. */

//	EditText question = null;
	TextView question = null;
	RadioButton answer1 = null;
	RadioButton answer2 = null;
	RadioButton answer3 = null;
	RadioButton answer4 = null;
	RadioGroup answers = null;
	Button finish = null;
	int selectedAnswer = -1;
	int quesIndex = 0;
	int qIndex;
	int numEvents = 0;
	int selected[] = null;
	int correctAns[] = null;
	boolean review =false;
	Button prev, next = null;

	TextView Suara;


	private MediaPlayer player = new MediaPlayer();
	private AssetFileDescriptor assetFile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question);

		TableLayout quizLayout = (TableLayout) findViewById(R.id.quizLayout);
		quizLayout.setVisibility(android.view.View.INVISIBLE);

		try {
			question = (TextView) findViewById(R.id.question);
			question.setOnClickListener(updateAnimalSound);
			answer1 = (RadioButton) findViewById(R.id.a0);
			answer1.setOnClickListener(updateAnimalSound);
			answer2 = (RadioButton) findViewById(R.id.a1);
			answer2.setOnClickListener(updateAnimalSound);
			answer3 = (RadioButton) findViewById(R.id.a2);
			answer3.setOnClickListener(updateAnimalSound);
			answer4 = (RadioButton) findViewById(R.id.a3);
			answer4.setOnClickListener(updateAnimalSound);
			answers = (RadioGroup) findViewById(R.id.answers);
			RadioGroup questionLayout = (RadioGroup)findViewById(R.id.answers);
			Button finish = (Button)findViewById(R.id.finish);
			finish.setOnClickListener(finishListener);

			prev = (Button)findViewById(R.id.Prev);
			prev.setOnClickListener(prevListener);
			next = (Button)findViewById(R.id.Next);
			next.setOnClickListener(nextListener);


			selected = new int[QuizFunActivity.getQuesList().length()];
			java.util.Arrays.fill(selected, -1);
			correctAns = new int[QuizFunActivity.getQuesList().length()];
			java.util.Arrays.fill(correctAns, -1);


			this.showQuestion(0,review);

			quizLayout.setVisibility(android.view.View.VISIBLE);

		} catch (Exception e) {
			Log.e("", e.getMessage().toString(), e.getCause());
		}

	}


	private void showQuestion(int qIndex,boolean review) {
		try {
			JSONObject aQues = QuizFunActivity.getQuesList().getJSONObject(qIndex);
			String quesValue = aQues.getString("Question");
			if (correctAns[qIndex] == -1) {
				String correctAnsStr = aQues.getString("CorrectAnswer");
				correctAns[qIndex] = Integer.parseInt(correctAnsStr);
			}

			question.setText(quesValue.toCharArray(), 0, quesValue.length());
			answers.check(-1);
			answer1.setTextColor(Color.WHITE);
			answer2.setTextColor(Color.WHITE);
			answer3.setTextColor(Color.WHITE);
			answer4.setTextColor(Color.WHITE);
			JSONArray ansList = aQues.getJSONArray("Answers");
			String aAns = ansList.getJSONObject(0).getString("Answer");
			answer1.setText(aAns.toCharArray(), 0, aAns.length());
			aAns = ansList.getJSONObject(1).getString("Answer");
			answer2.setText(aAns.toCharArray(), 0, aAns.length());
			aAns = ansList.getJSONObject(2).getString("Answer");
			answer3.setText(aAns.toCharArray(), 0, aAns.length());
			aAns = ansList.getJSONObject(3).getString("Answer");
			answer4.setText(aAns.toCharArray(), 0, aAns.length());
			Log.d("",selected[qIndex]+"");
			if (selected[qIndex] == 0)
				answers.check(R.id.a0);
			if (selected[qIndex] == 1)
				answers.check(R.id.a1);
			if (selected[qIndex] == 2)
				answers.check(R.id.a2);
			if (selected[qIndex] == 3)
				answers.check(R.id.a3);

			setScoreTitle();
			if (quesIndex == (QuizFunActivity.getQuesList().length()-1))
				next.setEnabled(false);

			if (quesIndex == 0)
				prev.setEnabled(false);

			if (quesIndex > 0)
				prev.setEnabled(true);

			if (quesIndex < (QuizFunActivity.getQuesList().length()-1))
				next.setEnabled(true);


			if (review) {
				Log.d("review",selected[qIndex]+""+correctAns[qIndex]);;
				if (selected[qIndex] != correctAns[qIndex]) {
					if (selected[qIndex] == 0)
						answer1.setTextColor(Color.RED);
					if (selected[qIndex] == 1)
						answer2.setTextColor(Color.RED);
					if (selected[qIndex] == 2)
						answer3.setTextColor(Color.RED);
					if (selected[qIndex] == 3)
						answer4.setTextColor(Color.RED);
				}
				if (correctAns[qIndex] == 0)
					answer1.setTextColor(Color.GREEN);
				if (correctAns[qIndex] == 1)
					answer2.setTextColor(Color.GREEN);
				if (correctAns[qIndex] == 2)
					answer3.setTextColor(Color.GREEN);
				if (correctAns[qIndex] == 3)
					answer4.setTextColor(Color.GREEN);
			}
		} catch (Exception e) {
			Log.e(this.getClass().toString(), e.getMessage(), e.getCause());
		}
	}


	private OnClickListener finishListener = new OnClickListener() {
		public void onClick(View v) {
			setAnswer();
			//Calculate Score
			int score = 0;
			for(int i=0; i<correctAns.length; i++){
				if ((correctAns[i] != -1) && (correctAns[i] == selected[i]))
					score++;
			}
			AlertDialog alertDialog;
			alertDialog = new AlertDialog.Builder(QuestionActivity.this).create();
			alertDialog.setTitle("Score");
			alertDialog.setMessage((score) +" out of " + (QuizFunActivity.getQuesList().length()));

			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Retake", new DialogInterface.OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					review = false;
					quesIndex=0;
					QuestionActivity.this.showQuestion(0, review);
				}
			});

			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Review", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					review = true;
					quesIndex = 0;
					QuestionActivity.this.showQuestion(0, review);
				}
			});

			alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Quit", new DialogInterface.OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					review = false;
					finish();
				}
			});

			alertDialog.show();

		}
	};

	private void setAnswer() {
		if (answer1.isChecked())
			selected[quesIndex] = 0;
		if (answer2.isChecked())
			selected[quesIndex] = 1;
		if (answer3.isChecked())
			selected[quesIndex] = 2;
		if (answer4.isChecked())
			selected[quesIndex] = 3;

		Log.d("",Arrays.toString(selected));
		Log.d("", Arrays.toString(correctAns));

	}

	private OnClickListener nextListener = new OnClickListener() {
		public void onClick(View v) {
			setAnswer();
			quesIndex++;
			if (quesIndex >= QuizFunActivity.getQuesList().length())
				quesIndex = QuizFunActivity.getQuesList().length() - 1;

			showQuestion(quesIndex,review);
		}
	};

	private OnClickListener prevListener = new OnClickListener() {
		public void onClick(View v) {
			setAnswer();
			quesIndex--;
			if (quesIndex < 0)
				quesIndex = 0;

			showQuestion(quesIndex,review);
		}
	};

	private void setScoreTitle() {
		this.setTitle("SciQuiz3     " + (quesIndex + 1) + "/" + QuizFunActivity.getQuesList().length());
	}

	@Override
	public void onClick(View v) {
		int i = 0;
		nextTrack(i);
		i++;
		player.start();

	}

	void nextTrack(int i)
	{
		JSONObject aQues = null;
		String quesValue = null;
		String answerValue = null;
		String aAns = null;
		try {
			aQues = QuizFunActivity.getQuesList().getJSONObject(quesIndex);
			JSONArray ansList = aQues.getJSONArray("Answers");
			//aAns = ansList.getJSONObject(qIndex).getString("Answer");

		//	quesValue = aQues.getString("Question");
			answerValue = aQues.getString("Answers");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//if(quesValue.equals(answerValue)){
			Toast.makeText(getApplicationContext(),aAns,Toast.LENGTH_LONG).show();
		//}
	}

	private OnClickListener updateAnimalSound = new OnClickListener(){

		public void onClick(View v) {
			JSONObject aQues = null;
			String quesValue = null;
			String answerValue = null;
			String aAns = null;
			try {
				aQues = QuizFunActivity.getQuesList().getJSONObject(quesIndex);
				JSONArray ansList = aQues.getJSONArray("Answers");
				if(v == answer1) {
					aAns = ansList.getJSONObject(0).getString("Answer");
				}else if (v == answer2){
					aAns = ansList.getJSONObject(1).getString("Answer");
				}else {
					aAns = ansList.getJSONObject(2).getString("Answer");
				}

				player.reset();
				assetFile = getAssets().openFd( "sounds/" + aAns.toString() + ".mp3" );
				player.setDataSource(assetFile.getFileDescriptor(), assetFile.getStartOffset(), assetFile.getLength());
				assetFile.close();
				player.prepare();
				player.start();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//if(quesValue.equals(answerValue)){

			//Toast.makeText(getApplicationContext(),aAns.toString(),Toast.LENGTH_LONG).show();

		}

	};

/*	private OnClickListener questionListener = new OnClickListener() {

		@Override
		public void onClick( View v ) {
			if ( v.getId() == Suara.getId() )
				player.start();
			try {
				player.reset();
				assetFile = getAssets().openFd( "sounds/" + huruf[currentHuruf].getSoundfile() );
				player.setDataSource( assetFile.getFileDescriptor(), assetFile.getStartOffset(), assetFile.getLength() );
				assetFile.close();
				player.prepare();
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onClick(View v) {
		if ( v.getId() == Suara.getId() )
			player.start();
	}*/
}